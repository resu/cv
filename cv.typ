#set text(size: 11pt)

#set page(
	paper: "a4",
	header: text(12pt, context [
		Valentino Di Giosaffatte · _Curriculum Vitae_
		#h(1fr)
		Page #counter(page).display("1/1", both: true) 
		#line(length: 100%, stroke: silver)
	]),
)

#let period(from, to) = { text(12pt, weight: "regular")[#from - #to] }

#let inline(key, ..vals) = {
	let v = vals.pos().join(", ")
	[#text(12pt)[*#key · *]#v]
}

#let tags(..vals) = {
	let v = vals.pos().join(" · ")
	text(12pt)[*#v*]
}

= Work

== Ph.D. Student #h(1fr) #period[Sep 2023][Now]
University of L'Aquila · L'Aquila \
XXXIX cycle · National Centre of HPC, Big Data and Quantum Computing \
EU - NextGenerationEU - PNRR \
_Supervisor_ · Henry Muccini \
_At_ · FrameLab Software Engineering Group \
_Project_ · On the use of Machine Learning for the discovery and rapid execution of QoS-aware microservices \

== Fellow Researcher #h(1fr) #period[Jun 2023][Aug 2023]
University of L'Aquila · L'Aquila \
_Supervisor_ · Henry Muccini \
_At_ · FrameLab Software Engineering Group \
_Project_ · Concurrent monitoring environments for distributed systems

== Ph.D. Candidate · winner #h(1fr) #period[Jul 2022][Dec 2022]
University of Italian Switzerland · Lugano \
_Supervisor_ · Walter Binder \
_At_ · Dynamic Analysis Group\
_Project_ · Dynamic x86 SIMD instructions profiler for the Hotspot VM using Intel Pin

= Education

== LM-18 · Master's Degree in Computer Science #h(1fr) #period[2018][Mar 2022]
University of L'Aquila \@ L'Aquila \
Department of Information Engineering, Computer Science and Mathematics \
_Supervisor_ · Fabrizio Rossi · 110/110 _cum laude_ \
_Dissertation_ · Models for incremental public transportation service design \

== MU1 · Level-1 Master Course - Mobile and Web Technologies #h(1fr) #period[2019][Dec 2020]
University of L'Aquila \@ L'Aquila \
Department of Information Engineering, Computer Science and Mathematics \
_Supervisor_ · Henry Muccini · 70/70 _cum laude_ \

== L-31 · Bachelor's Degree in Computer Science #h(1fr) #period[2013][Jul 2018]
University of L'Aquila \@ L'Aquila \
Department of Information Engineering, Computer Science and Mathematics \
_Supervisor_ · Gianpiero Monaco \
_Dissertation_ · Computation and performance of Nash equilibria on Generalized Graph K-Coloring game \

= Languages #v(0.5em)

- #inline("Italian", "Native")
- #inline("English", "C1")

#pagebreak()

= Interests #v(0.5em)

- #inline("Programming Languages", "Compilers", "Assemblers", "Debuggers", "Virtual Machines") \
- #inline("Operating Systems", "Kernels", "Hypervisors", "Distributed", "Embedded", "Linkers/Loaders") \
- #inline("Computer Graphics", "Rendering Engines", "Compositors", "Display Servers", "Window Managers") \

= Programming #v(0.5em)

#tags("C", "C++", "Go", "Rust", "Asm x86", "Python", "Lua", "Asm ARM", "Asm RiscV", "Zig", "Nix") 

= Projects #v(0.5em)

- #inline("dmon", "Monitoring system for Docker applications")
- #inline("wgen", "Workload generator for any network-reachable application")
- #inline("flow", "Workflow-driven experiment controller for dmon and wgen")
- #inline("dxvc", "Intel Pin dynamic profiler for counting the number of executed SIMD instructions")
